import './styles.css';
import paisagem from '../../imgs/paisagem.png';
import footer_banner from '../../imgs/footer_banner.svg';

export function Banner({ imagem }) {
  return (
    <div className="container-banner">
      <img src={footer_banner} alt="Image1" className="secondary-image" />
      <img src={imagem ? imagem : paisagem} alt="Image2" className="principal-image" />
    </div>
  )
}