import './styles.css';
import Logo from '../../imgs/logo.svg';
import { Link } from 'react-router-dom';


export function Header() {
  return (
    <div className="container-header">
      <img src={Logo} alt="Logo" className="logo-header" />
      <ul className="menu-header">
        <Link to="/"><li>HOME</li></Link>
        <Link to="/serra-nova"><li>SERRA NOVA</li></Link>
        <Link to="/atrativos"> <li>ATRATIVOS</li></Link>
        <Link to="/hospedagens"><li>HOSPEDAGENS</li></Link>
        <Link to="/gastronomia"><li>GASTRONOMIA</li></Link>
        <Link to="/eventos"><li>EVENTOS</li></Link>
        <Link to="/guias-turisticos"><li>GUIAS TURÍSTICOS</li></Link>
        <Link to="/contato"><li>CONTATO</li></Link>
      </ul>
    </div>
  )
}