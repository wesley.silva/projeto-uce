import "./styles.css";
import logoport from "../../imgs/logoport.svg";
import facebook from "../../imgs/facebook.svg";
import instagram from "../../imgs/instagram.svg";
import youtube from "../../imgs/youtube.svg";
import { Link } from "react-router-dom";

export function Footer() {
  return (
    <div className="container-footer">
      <div>
        <img src={logoport} alt="Logo Porteirinha" />
      </div>
      <div className="visite-porteirinha-footer">
        <span>Visite Porteirinha ;)</span>
      </div>
      <div className="redes-sociais-footer">
        <strong>Redes Sociais</strong>
        <ul>
          <li>
            <img src={facebook} alt="Facebook" />
            <span>Facebook</span>
          </li>
          <li>
            <img src={instagram} alt="instagram" />
            <span>Instagram</span>
          </li>
          <li>
            <img src={youtube} alt="youtube" />
            <span>Youtube</span>
          </li>

          <Link to="/login" style={{ marginTop: 30 }}>
            <span style={{ color: "#FFF" }}>Login</span>
          </Link>
        </ul>
      </div>
    </div>
  );
}
