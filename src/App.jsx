import { BrowserRouter, Switch, Route } from "react-router-dom";
import { Home, SerraNova, Atrativos, Login } from "./pages";

import { Header } from "./components";
import { Hospedagem } from "./pages/Hospedagens";
import { Gastronomia } from "./pages/Gastronomia";
import { Eventos } from "./pages/Eventos";
import { Guias } from "./pages/GuiasTuristicos";
import { Contato } from "./pages/Contato";

export default function Routes() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/serra-nova" component={SerraNova} />
        <Route path="/atrativos" component={Atrativos} />
        <Route path="/hospedagens" component={Hospedagem} />
        <Route path="/gastronomia" component={Gastronomia} />
        <Route path="/eventos" component={Eventos} />
        <Route path="/guias-turisticos" component={Guias} />
        <Route path="/contato" component={Contato} />
        <Route path="/login" component={Login} />
        <Route path="*" component={() => <h1>Page not found</h1>} />
      </Switch>
    </BrowserRouter>
  );
}
