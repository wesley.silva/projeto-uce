import { Banner, Footer } from "../../components";

import draw1 from "../../imgs/draw1.svg";
import draw2 from "../../imgs/draw2.svg";
import paisagem from "../../imgs/paisagem.png";
import food from "../../imgs/food.svg";
import bed from "../../imgs/bed.svg";
import calendar from "../../imgs/calendar.svg";

import "./styles.css";

export function Home() {
  return (
    <>
      <Banner />
      <section className="section-phrase-home">
        <p className="phrase-home">
          Nulla vitae dolor hendrerit, sagittis purus ut, tristique erat.
          Quisque laoreet dui sed mi venenatis molestie vitae at urna
        </p>
        <img src={draw1} alt="Draw1" className="draw1-home" />
        <img src={draw2} alt="Draw2" className="draw2-home" />
      </section>
      <section className="rotas-turisticas-home">
        <div>
          <img src={paisagem} alt="paisagem" className="card-paisagem-home" />
          <span>Serrado</span>
        </div>
        <div>
          <img src={paisagem} alt="paisagem" className="card-paisagem-home" />
          <span>Talhado</span>
        </div>
        <div>
          <img src={paisagem} alt="paisagem" className="card-paisagem-home" />
          <span>Cachoeira Maria Rosa</span>
        </div>
      </section>
      <section className="section-services-home">
        <div className="container-service-home">
          <div>
            <img src={food} alt="Food" />
          </div>
          <span>Gastronomia</span>
        </div>

        <div className="container-service-home">
          <div>
            <img src={bed} alt="bed" />
          </div>
          <span>Hospedagem</span>
        </div>

        <div className="container-service-home">
          <div>
            <img src={calendar} alt="calendar" />
          </div>
          <span>Eventos</span>
        </div>
      </section>
      <section className="section-phrase-home">
        <p className="phrase-home">
          Nulla vitae dolor hendrerit, sagittis purus ut, tristique erat.
          Quisque laoreet dui sed mi venenatis molestie vitae at urna
        </p>
      </section>

      <Footer />
    </>
  );
}
