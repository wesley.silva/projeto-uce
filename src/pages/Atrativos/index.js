import React, { useState, useEffect } from "react";
import { Banner, Footer } from "../../components";
import firebase from "../../firebase";

import "./styles.css";

export function Atrativos() {
  const [atrativos, setAtrativos] = useState([]);

  useEffect(() => {
    firebase
      .database()
      .ref("/atrativos")
      .on("value", function (snapshot) {
        var newData = [...atrativos];
        snapshot.forEach(function (data) {
          let temp = {};
          temp = { ...data.val() };
          temp.id = data.key;
          newData.push(temp);
        });
        setAtrativos(newData);
      });
  }, []);

  if (atrativos.length === 0) {
    return null;
  }

  return (
    <>
      <Banner />
      <div className="container-atrativos">
        <h1>Atrativos</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac
          massa at nisl scelerisque posuere id at ex. Maecenas convallis augue
          neque, in ornare turpis aliquet id. Quisque dolor lorem, lacinia quis
          mi at, facilisis vulputate nibh. Quisque porttitor porttitor elit quis
          scelerisque. Curabitur volutpat volutpat eros et interdum. Proin
          pretium ipsum vitae rhoncus imperdiet. Ut id imperdiet nisi, ac
          bibendum purus. Nullam cursus, risus at pharetra congue, neque ipsum
          pharetra ipsum, non finibus neque nulla at sapien. Cras dignissim
          elementum sagittis. Proin euismod nisl quis libero scelerisque
          bibendum. Mauris nec velit non neque finibus mollis hendrerit.
        </p>
      </div>

      <section className="rotas-turisticas-home">
        {atrativos.map((atrativo) => {
          return (
            <div key={atrativo.id}>
              <img
                src={atrativo.imageURL}
                alt="paisagem"
                className="card-paisagem-home"
              />
              {/*               <div> */}
              <span>{atrativo.name}</span>
              {/* </div> */}
            </div>
          );
        })}
      </section>
      <Footer />
    </>
  );
}
