import { Banner, Footer } from "../../components";

import paisagem from '../../imgs/paisagem.png';

import './styles.css';

export function SerraNova() {
  return (
    <>
      <Banner />
      <div className="container-serra-nova">
        <h1>Serra Nova</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac massa at nisl scelerisque posuere id at ex. Maecenas convallis augue neque, in ornare turpis aliquet id. Quisque dolor lorem, lacinia quis mi at, facilisis vulputate nibh. Quisque porttitor porttitor elit quis scelerisque. Curabitur volutpat volutpat eros et interdum. Proin pretium ipsum vitae rhoncus imperdiet. Ut id imperdiet nisi, ac bibendum purus. Nullam cursus, risus at pharetra congue, neque ipsum pharetra ipsum, non finibus neque nulla at sapien. Cras dignissim elementum sagittis. Proin euismod nisl quis libero scelerisque bibendum. Mauris nec velit non neque finibus mollis hendrerit.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac massa at nisl scelerisque posuere id at ex. Maecenas convallis augue neque, in ornare turpis aliquet id. Quisque dolor lorem, lacinia quis mi at, facilisis vulputate nibh. Quisque porttitor porttitor elit quis scelerisque. Curabitur volutpat volutpat eros et interdum. Proin pretium ipsum vitae rhoncus imperdiet. Ut id imperdiet nisi, ac bibendum purus. Nullam cursus, risus at pharetra congue, neque ipsum pharetra ipsum, non finibus neque nulla at sapien. Cras dignissim elementum sagittis. Proin euismod nisl quis libero scelerisque bibendum. Mauris nec velit non neque finibus mollis hendrerit.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac massa at nisl scelerisque posuere id at ex. Maecenas convallis augue neque, in ornare turpis aliquet id. Quisque dolor lorem, lacinia quis mi at, facilisis vulputate nibh. Quisque porttitor porttitor elit quis scelerisque. Curabitur volutpat volutpat eros et interdum. Proin pretium ipsum vitae rhoncus imperdiet. Ut id imperdiet nisi, ac bibendum purus. Nullam cursus, risus at pharetra congue, neque ipsum pharetra ipsum, non finibus neque nulla at sapien. Cras dignissim elementum sagittis. Proin euismod nisl quis libero scelerisque bibendum. Mauris nec velit non neque finibus mollis hendrerit.</p>
        <img src={paisagem} alt="Paisagem" />
      </div>
      <Footer />
    </>

  )
}