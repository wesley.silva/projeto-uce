import React, { useState, useEffect } from "react";
import firebase from "../../firebase";
import { Banner, Footer } from "../../components";
import gastronomia from "../../imgs/gastronomia.svg";

import "./styles.css";

export function Gastronomia() {
  const [data, setData] = useState([]);

  useEffect(() => {
    firebase
      .database()
      .ref("/gastronomia")
      .on("value", function (snapshot) {
        var newData = [...data];
        snapshot.forEach(function (data) {
          let temp = {};
          temp = { ...data.val() };
          temp.id = data.key;
          newData.push(temp);
        });
        setData(newData);
      });
  }, []);

  if (data.length === 0) {
    return null;
  }

  return (
    <>
      <Banner imagem={gastronomia} />
      <div className="container-atrativos">
        <h1>Gastronomia</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac
          massa at nisl scelerisque posuere id at ex. Maecenas convallis augue
          neque, in ornare turpis aliquet id. Quisque dolor lorem, lacinia quis
          mi at, facilisis vulputate nibh. Quisque porttitor porttitor elit quis
          scelerisque. Curabitur volutpat volutpat eros et interdum. Proin
          pretium ipsum vitae rhoncus imperdiet. Ut id imperdiet nisi, ac
          bibendum purus. Nullam cursus, risus at pharetra congue, neque ipsum
          pharetra ipsum, non finibus neque nulla at sapien. Cras dignissim
          elementum sagittis. Proin euismod nisl quis libero scelerisque
          bibendum. Mauris nec velit non neque finibus mollis hendrerit.
        </p>
      </div>
      <section className="rotas-turisticas-home">
        {data.map((item) => {
          return (
            <div key={item.id}>
              <img
                src={item.imageURL}
                alt="paisagem"
                className="card-paisagem-home"
              />

              <span>{item.name}</span>
            </div>
          );
        })}
      </section>
      <Footer />
    </>
  );
}
