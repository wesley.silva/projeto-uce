import React, { useState, useEffect } from "react";
import firebase from "../../firebase";
import { Banner, Footer } from "../../components";
import guia from "../../imgs/guia.svg";

import locale from "../../imgs/locale.svg";
import calendario from "../../imgs/calendario.svg";

import "./styles.css";

export function Guias() {
  const [data, setData] = useState([]);
  useEffect(() => {
    firebase
      .database()
      .ref("/guias")
      .on("value", function (snapshot) {
        var newData = [...data];
        snapshot.forEach(function (data) {
          let temp = {};
          temp = { ...data.val() };
          temp.id = data.key;
          newData.push(temp);
        });
        setData(newData);
      });
  }, []);

  if (data.length === 0) {
    return null;
  }

  return (
    <>
      <Banner imagem={guia} />
      <div className="container-events">
        <h1>Guias Turísticos</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ac
          massa at nisl scelerisque posuere id at ex. Maecenas convallis augue
          neque, in ornare turpis aliquet id. Quisque dolor lorem, lacinia quis
          mi at, facilisis vulputate nibh. Quisque porttitor porttitor elit quis
          scelerisque. Curabitur volutpat volutpat eros et interdum. Proin
          pretium ipsum vitae rhoncus imperdiet. Ut id imperdiet nisi, ac
          bibendum purus. Nullam cursus, risus at pharetra congue, neque ipsum
          pharetra ipsum, non finibus neque nulla at sapien. Cras dignissim
          elementum sagittis. Proin euismod nisl quis libero scelerisque
          bibendum. Mauris nec velit non neque finibus mollis hendrerit.
        </p>
      </div>
      <section className="card-events-home">
        {data.map((item) => {
          return (
            <div key={item.id} className="container-card-home">
              <img
                style={{
                  padding: 10,
                  borderRadius: 8,
                  objectFit: "cover",
                  objectPosition: "top",
                }}
                src={item.imageURL}
                alt="paisagem"
                /*                 className="card-paisagem-home" */
              />
              <span
                style={{
                  fontSize: 18,
                  fontWeight: 700,
                  textTransform: "uppercase",
                  color: "#000",
                  textAlign: "center",
                  marginTop: 10,
                }}
              >
                {item.name}
              </span>
              <div className="box-card">
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: 10,
                  }}
                >
                  <img
                    src={calendario}
                    style={{ marginRight: 10, width: 20, height: 20 }}
                  />
                  <p>{item.phone}</p>
                </div>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    marginBottom: 10,
                  }}
                >
                  <img
                    src={locale}
                    style={{ marginRight: 10, width: 20, height: 20 }}
                  />
                  <p>@{item.instagram}</p>
                </div>
              </div>
            </div>
          );
        })}
      </section>
      <Footer />
    </>
  );
}
