import React, { useState } from "react";

export function Login() {
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  return (
    <div
      style={{
        display: "flex",
        flex: 1,
        height: "85vh",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}
    >
      <form
        style={{
          flexDirection: "column",
          display: "flex",
          borderWidth: 1,
          borderColor: "#aaa",
          borderStyle: "solid",
          paddingTop: 50,
          paddingBottom: 150,
          paddingLeft: 50,
          paddingRight: 50,
          borderRadius: 10,
        }}
      >
        <h1 style={{ marginBottom: 20, textAlign: "center", color: "#444" }}>
          Login
        </h1>
        <input
          placeholder="Email"
          style={{
            fontFamily: "Montserrat",
            fontSize: 16,
            width: 300,
            height: 40,
            marginTop: 10,
            paddingLeft: 5,
            paddingRight: 5,
          }}
        />
        <input
          type="password"
          placeholder="Senha"
          style={{
            fontSize: 16,
            fontFamily: "Montserrat",
            width: 300,
            height: 40,
            marginTop: 10,
            paddingLeft: 5,
            paddingRight: 5,
          }}
        />

        <button
          style={{
            fontSize: 16,
            marginTop: 30,
            height: 40,
            color: "#fff",
            border: 0,
            background: "#1B1464",
          }}
        >
          Entrar
        </button>
      </form>
    </div>
  );
}
