import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyD6tYISSZRmj1L89FBd76rQwPnksbSR058",
  authDomain: "uce-project.firebaseapp.com",
  databaseURL: "https://uce-project-default-rtdb.firebaseio.com",
  projectId: "uce-project",
  storageBucket: "uce-project.appspot.com",
  messagingSenderId: "472003626822",
  appId: "1:472003626822:web:780099983f84af59b80747",
};

export default firebase.initializeApp(firebaseConfig);
